/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** ACharacter
*/

#include "Character/ACharacter.hpp"

namespace indie {
    void ACharacter::setAnimation(irr::scene::IAnimatedMesh *mesh, const irr::core::vector3df &pos,
                                  const irr::core::vector3df &rot) {
        if (node_->getMesh() == mesh)
            return;
        node_->setMesh(mesh);
        node_->setMaterialTexture(0, SceneManager_->getVideoDriver()->getTexture(texture_.c_str()));
    }

    void ACharacter::standUp() {
        irr::core::vector3df node_position = getPlayerPosition();
        irr::core::vector3df node_rotation = getPlayerRotation();
        setAnimation(stand_mesh_, node_position, node_rotation);
        setRotation(node_rotation);
        setPlayerPosition(node_position);
    }

    void ACharacter::move(float new_rot, Directions direction) {
        irr::core::vector3df node_position = getPlayerPosition();
        irr::core::vector3df node_rotation = getPlayerRotation();
        switch (direction) {
            case Directions::DOWN: {
                node_position.X += MaxSpeed_;
                break;
            }
            case Directions::UP: {
                node_position.X -= MaxSpeed_;
                break;
            }
            case Directions::LEFT: {
                node_position.Z -= MaxSpeed_;
                break;
            }
            case Directions::RIGHT: {
                node_position.Z += MaxSpeed_;
                break;
            }
        }
        node_rotation.Y = new_rot;
        setAnimation(run_mesh_, node_position, node_rotation);
        setRotation(node_rotation);
        setPlayerPosition(node_position);
    }

    bool ACharacter::addBomb(irr::scene::ISceneNode *parent,
                             irr::scene::ISceneManager *mgr, irr::s32 id = -1,
                             const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0)) {
        if (bombs_.size() < MaxBomb_) {
            bombs_.push_back(std::make_unique<Bomb>(parent, mgr, id, position));
            return true;
        }
        return false;
    }

    void ACharacter::LevelUpMaxBomb() {
        MaxBomb_ += 1;
    }

    void ACharacter::LevelUpMaxSpeed() {
        if (MaxSpeed_ < 0.40)
            MaxSpeed_ += 0.04;
    }

    void ACharacter::LevelUpRange() {
        if (Range_ < 5)
            Range_ += 1;
    }

}