/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** IA
*/

#include "Character/IA.hpp"

namespace indie {

    void IA::hide(std::vector<std::string> map, int hide, Directions dir) {
        if (dir == UP) {
            if (getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
                return;
            } else if (getCenter_().X / 2 + hide <= 12 && map[getCenter_().X / 2 + hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
                return;
            } else if (getCenter_().Z / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - hide);
                direction_ = LEFT;
                return;
            } else if (getCenter_().Z / 2 + hide <= 15 && map[getCenter_().X / 2][getCenter_().Z / 2 + hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + hide);
                direction_ = RIGHT;
                return;
            }
            if (map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - 1);
                direction_ = LEFT;
            } else if (map[getCenter_().X / 2][getCenter_().Z / 2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + 1);
                direction_ = RIGHT;
            } else if (map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
            } else {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
            }
        } else if (dir == DOWN) {
            if (getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
                return;
            } else if (getCenter_().X / 2 + hide <= 12 && map[getCenter_().X / 2 + hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
                return;
            } else if (getCenter_().Z / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - hide);
                direction_ = LEFT;
                return;
            } else if (getCenter_().Z / 2 + hide <= 15 && map[getCenter_().X / 2][getCenter_().Z / 2 + hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X/2][getCenter_().Z/2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + hide);
                direction_ = RIGHT;
                return;
            }
            if (map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - 1);
                direction_ = LEFT;
            } else if (map[getCenter_().X / 2][getCenter_().Z / 2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + 1);
                direction_ = RIGHT;
            } else if (map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
            } else {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
            }
        } else if (dir == LEFT) {
            if (getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
                return;
            } else if (getCenter_().X / 2 + hide <= 12 && map[getCenter_().X / 2 + hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
                return;
            } else if (getCenter_().Z / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - hide);
                direction_ = LEFT;
                return;
            } else if (getCenter_().Z / 2 + hide <= 15 && map[getCenter_().X / 2][getCenter_().Z / 2 + hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z /2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + hide);
                direction_ = RIGHT;
                return;
            }
            if (map[getCenter_().X / 2 - 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
            } else if (map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
            } else if (map[getCenter_().X / 2][getCenter_().Z / 2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + 1);
                direction_ = RIGHT;
            } else {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - 1);
                direction_ = LEFT;
            }
        } else {
            if (getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 - 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
                return;
            } else if (getCenter_().X / 2 + hide <= 12 && map[getCenter_().X / 2 + hide][getCenter_().Z / 2] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + hide, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
                return;
            } else if (getCenter_().Z / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - hide);
                direction_ = LEFT;
                return;
            } else if (getCenter_().Z / 2 + hide <= 15 && map[getCenter_().X / 2][getCenter_().Z / 2 + hide] == ' ' &&
                getCenter_().X / 2 - hide >= 0 && map[getCenter_().X/2][getCenter_().Z/2 + 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + hide);
                direction_ = RIGHT;
                return;
            }
            if (map[getCenter_().X / 2 - 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 - 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = UP;
            } else if (map[getCenter_().X / 2 + 1][getCenter_().Z / 2] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2 + 1, -1,
                                                  getCenter_().Z / 2);
                direction_ = DOWN;
            } else if (map[getCenter_().X / 2][getCenter_().Z / 2 - 1] == ' ') {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 - 1);
                direction_ = LEFT;
            } else {
                objective_ = irr::core::vector3df(getCenter_().X / 2, -1,
                                                  getCenter_().Z / 2 + 1);
                direction_ = RIGHT;
            }
        }
    }

        bool IA::checkDanger(std::vector<std::string> map) {
            safe_ = false;
            if (keep_)
                return false;
            for (int r = 0; r <= 2; r++) {
                if (getCenter_().X / 2 - r >= 0 && map[getCenter_().X / 2 - r][getCenter_().Z / 2] == 'o') {
                    hide(map, 3 - r, UP);
                    keep_ = true;
                    return true;
                }
                if (getCenter_().X / 2 + r <= 12 && map[getCenter_().X / 2 + r][getCenter_().Z / 2] == 'o') {
                    hide(map, 3 - r, DOWN);
                    keep_ = true;
                    return true;
                }
                if (getCenter_().Z / 2 - r >= 0 && map[getCenter_().X / 2][getCenter_().Z / 2 - r] == 'o') {
                    hide(map, 3 - r, LEFT);
                    keep_ = true;
                    return true;
                }
                if (getCenter_().Z / 2 + r <= 15 && map[getCenter_().X / 2][getCenter_().Z / 2 + r] == 'o') {
                    hide(map, 3 - r, RIGHT);
                    keep_ = true;
                    return true;
                }
            }
            return false;
        }

    }