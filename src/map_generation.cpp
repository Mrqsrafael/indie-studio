/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** main
*/

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>

void iron_line(std::ofstream &mapFile)
{
	for (int n = 0; n < 15; n++)
		mapFile << "X";
	mapFile << std::endl;
}

void empty_line(std::ofstream &mapFile)
{
	mapFile << "X";
	for (int i = 0; i < 13; i++) {
		if ((std::rand() % 20) > 3)
			mapFile << "*";
		else
			mapFile << " ";
	}
	mapFile << "X" << std::endl;
}

void starter_mix_line(std::ofstream &mapFile)
{
	mapFile << "X";
	mapFile << " ";
	mapFile << "X";
	for (int i = 0; i < 5; i++) {
		if ((std::rand() % 20) > 2)
			mapFile << "*";
		else
			mapFile << " ";
		mapFile << "X";
	}
	mapFile << " ";
	mapFile << "X" << std::endl;
}

void classic_line(std::ofstream &mapFile)
{
	mapFile << "X";
	for (int i = 0; i < 6; i++) {
		if ((std::rand() % 20) > 2)
			mapFile << "*";
		else
			mapFile << " ";
		mapFile << "X";
	}
		if ((std::rand() % 20) > 2)
			mapFile << "*";
		else
			mapFile << " ";
	mapFile << "X" << std::endl;
}

void player_line(std::ofstream &mapFile)
{
	int randVar = 0;

	mapFile << "X";
	mapFile << "P";
	mapFile << " ";
	for (int i = 0; i < 9; i++) {
		randVar = (std::rand() % 20);
		if ((std::rand() % 20) > 3)
			mapFile << "*";
		else
			mapFile << " ";
	}
	mapFile << " ";
	mapFile << "P";
	mapFile << "X" << std::endl;
}

void print_map(std::ofstream &mapFile)
{
	iron_line(mapFile);
	player_line(mapFile);
	starter_mix_line(mapFile);
	for (int i = 0; i < 4; i++) {
		empty_line(mapFile);
		if (i != 3)
			classic_line(mapFile);
	}
	starter_mix_line(mapFile);
	player_line(mapFile);
	iron_line(mapFile);
}

void map_generation()
{
	std::ofstream mapFile;
	mapFile.open("map.txt");
	std::srand(std::time(nullptr));
	print_map(mapFile);
	mapFile.close();
}