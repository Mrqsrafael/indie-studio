/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Loose
*/

#include "Scene/Loose.hpp"
#include "Core/Core.hpp"

namespace indie
{
	int Loose::manage_event(const EventReceiver &event_receiver) {
		if (buttonList["Menu"]->isPressed()) {
			sound_.playSound("select");
			return (sceneAction::popGame);
		}
        if (buttonList["Quit"]->isPressed()) {
            sound_.playSound("select");
            return (sceneAction::popAll);
        }
        if (buttonList["Restart"]->isPressed()) {
            sound_.playSound("select");
            return (sceneAction::Restart);
        }
		return 0;
	}

	void Loose::printScore() {
	}

	void Loose::create_gui(irr::IrrlichtDevice *device) {
		this->setGui(device->getGUIEnvironment());
		irr::gui::IGUISkin* skin = this->getGui()->getSkin();
		irr::gui::IGUIFont* font = this->getGui()->getFont("media/font.png");

		if (font)
			skin->setFont(font);
		image_ = this->getGui()->addImage(device->getVideoDriver()->getTexture("media/GameOver.jpg"), {0,0});
		imageList["backgroundLoose"] = image_;
		skin->setFont(this->getGui()->getBuiltInFont(), irr::gui::EGDF_TOOLTIP);
		skin->setColor(irr::gui::EGDC_3D_FACE, irr::video::SColor(200, 255, 45, 45));
        button_ = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,800 - 120,1920/2 + 300,800 - 80), 0, 1,
                                            L"Restart");
        buttonList["Restart"] = button_;
        button_ = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,800 - 60,1920/2 + 300,800 - 20), 0, 1,
                                            L"Back to Menu");
        buttonList["Menu"] = button_;
		button_ = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,800 ,1920/2 + 300,800 + 40), 0, 3,
			L"Quit the game");
		buttonList["Quit"] = button_;
	}

	void Loose::draw() {
		scene_manager_->drawAll();
		image_->draw();
		std::for_each(this->buttonList.begin(), this->buttonList.end(),
		[](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
			p.second->draw();
		});
	}
}
