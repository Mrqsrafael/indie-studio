/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** GamePause
*/

#include "Scene/GamePause.hpp"
#include "Core/Core.hpp"

namespace indie
{
    GamePause::GamePause(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) : AScene(device, s, p) {
        create_gui(device);
    }

    GamePause::~GamePause() {
        std::for_each(this->buttonList.begin(), this->buttonList.end(),
            [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                if (p.second)
                    p.second->remove();
            });
        std::for_each(this->imageList.begin(), this->imageList.end(),
            [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
                if (p.second)
                    p.second->remove();
        });
    }
    
    int GamePause::manage_event(const EventReceiver &event_receiver) {
        if (buttonList["Resume"]->isPressed()) {
            sound_.playSound("select");
            scene_manager_->addCameraSceneNode(scene_manager_->getRootSceneNode(),
                                               irr::core::vector3df(21,21, 14),
                                               irr::core::vector3df(13,0,14));
            return (sceneAction::popScene);
        }
        if (buttonList["Option"]->isPressed()) {
            sound_.stopSound(params_->getMusic());
            sound_.playSound("select");
            return (sceneAction::addOption);
        }
        if (buttonList["Quit and save"]->isPressed()) {
            sound_.playSound("confirm");
            return (sceneAction::popGame);
        }
        if (buttonList["Quit"]->isPressed()) {
            sound_.playSound("confirm");
            return (sceneAction::popGame);
        }
        return 0;
    }

    void GamePause::create_gui(irr::IrrlichtDevice *device) {
        this->setGui(device->getGUIEnvironment());
        irr::gui::IGUISkin* skin = this->getGui()->getSkin();
        irr::gui::IGUIFont* font = this->getGui()->getFont("media/font.png");
        irr::gui::IGUIButton *button;
        irr::gui::IGUIImage *image;

        if (font)
            skin->setFont(font);

        image = this->getGui()->addImage(device->getVideoDriver()->getTexture("media/background_menu"), {0,0});
        imageList["backgroundGamePause"] = image;
        skin->setFont(this->getGui()->getBuiltInFont(), irr::gui::EGDF_TOOLTIP);
        skin->setColor(irr::gui::EGDC_3D_FACE, irr::video::SColor(200, 201, 65, 178));
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 - 120,1920/2 + 300,1080/2 - 80), 0, 1,
            L"Resume");
        buttonList["Resume"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 - 60,1920/2 + 300,1080/2 - 20), 0, 2,
            L"Option");
        buttonList["Option"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 ,1920/2 + 300,1080/2 + 40), 0, 3,
            L"Quit");
        buttonList["Quit"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 + 60,1920/2 + 300,1080/2 + 100), 0, 3,
            L"Quit and save");
        buttonList["Quit and save"] = button;
    }

        void GamePause::draw() {
            scene_manager_->drawAll();
            std::for_each(this->imageList.begin(), this->imageList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
                    p.second->draw();
            });
            std::for_each(this->buttonList.begin(), this->buttonList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                    p.second->draw();
            });
    }
}