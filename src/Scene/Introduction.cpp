//
// Created by mrafael on 6/13/19.
//


#include "Scene/Introduction.hpp"
#include "Core/Core.hpp"

namespace indie
{
    void Introduction::put_bomb()
    {
        static bool pos = false;
        auto now = std::chrono::system_clock::now();
        std::chrono::duration<float> elapsed_seconds = now - clock_;
        float z = std::rand() % 45;

        if (elapsed_seconds.count() < 0.4)
            return;
        if (bombs_.size() < 10) {
            if (pos) {
                pos = !pos;
            } else {
                z = -z;
                pos = !pos;
            }
            bombs_.push_back(
                    std::make_unique<BombM>(scene_manager_->getRootSceneNode(), scene_manager_, 1, irr::core::vector3df(30, 20, z),
                            irr::core::vector3df(0, 0, 0),
                            irr::core::vector3df(10.0f, 10.0f, 10.0f)));
        }
        clock_ = now;
    }

    void Introduction::character_walk()
    {
        auto now = std::chrono::system_clock::now();
        std::chrono::duration<float> elapsed_seconds = now - walk_clock_;
        irr::core::vector3df next_pos = animated_node_->getPosition();

        if (elapsed_seconds.count() > 0.1) {
            next_pos.Z += 2;
            animated_node_->setPosition(next_pos);
        }
    }


    void Introduction::erase_bombs()
    {
        while (!bombs_.empty())
            bombs_.pop_back();
    }

    int Introduction::manage_event(const EventReceiver &event_receiver)
    {
        if (animated_node_ == nullptr)
            create_scene();
        put_bomb();
        if (start_)
            character_walk();
        for (auto it = bombs_.rbegin(); it != bombs_.rend(); it++) {
            if ((*it)->checkTimer() && !(*it)->used) {
                (*it)->doParticles();
                sound_.playSound("bomb_ex");
            }
            if ((*it)->used && (*it)->checkParticle()) {
                std::advance(it, 1);
                bombs_.erase(it.base());
                std::advance(it, -1);
            }
        }
        if (event_receiver.IsKeyDown(irr::EKEY_CODE::KEY_SPACE) || event_receiver.IsKeyDown(irr::EKEY_CODE::KEY_RETURN))
            start_ = true;
        if (animated_node_->getPosition().Z > 60) {
            start_ = false;
            animated_node_->remove();
            animated_node_ = nullptr;
            erase_bombs();
            return (sceneAction::addMenu);
        }
        if (event_receiver.IsKeyDown(irr::EKEY_CODE::KEY_ESCAPE))
            return (sceneAction::popScene);
        return 0;
    }

    void Introduction::create_gui(irr::IrrlichtDevice *device)
    {
        this->setGui(device->getGUIEnvironment());
        irr::gui::IGUISkin *skin = this->getGui()->getSkin();
        irr::gui::IGUIFont *font = this->getGui()->getFont("media/bm.bmp");

        if (font) {
            skin->setFont(font);
        }
        skin->setFont(this->getGui()->getBuiltInFont(), irr::gui::EGDF_DEFAULT);
        skin->setColor(irr::gui::EGDC_3D_FACE, irr::video::SColor(255, 0, 132, 180));
        image_ = this->getGui()->addImage(device->getVideoDriver()->getTexture("media/download.jpeg"), {0, 0});
    }

    void Introduction::create_scene()
    {
        animated_node_ = scene_manager_->addAnimatedMeshSceneNode(
                scene_manager_->getMesh("media/bomberman_run.b3d"),
                scene_manager_->getRootSceneNode(), -1,
                irr::core::vector3df(30, -35, -50),
                irr::core::vector3df(0, 150.0, 0),
                irr::core::vector3df(8.0f, 8.0f, 8.0f)
                );
        animated_node_->setMaterialTexture(0, scene_manager_->getVideoDriver()->getTexture("media/bomberman_black.png"));
        animated_node_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
        scene_manager_->addCameraSceneNode(scene_manager_->getRootSceneNode(),
                irr::core::vector3df(80.0f, 0.0f, 0.0f),
                irr::core::vector3df(0.0f, 0.0f, 0.0f));
    }

    void Introduction::draw()
    {
        image_->draw();
        scene_manager_->drawAll();
    }
}