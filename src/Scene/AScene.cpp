/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** AScene
*/

#include "Scene/AScene.hpp"

namespace indie
{
	AScene::AScene(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> parameter)
	    :   device_(device),
	        sound_(s),
	        params_(parameter)
	{
	    device_ = device;
		scene_manager_ = device->getSceneManager();
		scene_manager_->addCameraSceneNode(0,
				irr::core::vector3df(80.0f, 60.0f, 0.0f),
				irr::core::vector3df(0.0f, 0.0f, 0.0f));
    }

	void AScene::draw()
	{
		scene_manager_->drawAll();
	}
}
