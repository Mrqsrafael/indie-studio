/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Option
*/

#include <thread>

#include "Scene/Option.hpp"
#include "Core/Core.hpp"

namespace indie
{
    Option::Option(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) : AScene(device, s, p) {
        create_gui(device);
    }

    Option::~Option() {
		std::for_each(this->scroolList.begin(), this->scroolList.end(),
        [](const std::pair<std::string,  irr::gui::IGUIScrollBar *> &p) {
            p.second->remove();
        });
		std::for_each(this->buttonList.begin(), this->buttonList.end(),
        [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
            p.second->remove();
        });
		std::for_each(this->textList.begin(), this->textList.end(),
        [](const std::pair<std::string,  irr::gui::IGUIStaticText *> &p) {
            p.second->remove();
        });
        std::for_each(this->imageList.begin(), this->imageList.end(),
        [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
            p.second->remove();
        });
			}
    int Option::manage_event(const EventReceiver &event_receiver) {
        if (buttonList["Return"]->isPressed()) {
            sound_.playSound("select");
            scene_manager_->addCameraSceneNode(scene_manager_->getRootSceneNode(),
                                               irr::core::vector3df(21,21, 14),
                                               irr::core::vector3df(13,0,14));
            sound_.playSound(params_->getMusic());
            return (sceneAction::popScene);
        }
        if (buttonList["nextSound"]->isPressed()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            sound_.stopSound(params_->getMusic());
            params_->setMusic(1);
            sound_.playSound(params_->getMusic());
        } else if (buttonList["prevSound"]->isPressed()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            sound_.stopSound(params_->getMusic());
            params_->setMusic(-1);
            sound_.playSound(params_->getMusic());
        }
        params_->setMusicVolume(scroolList["ScroolMusicVolume"]->getPos());
        params_->setEffectVolume(scroolList["ScroolEffectVolume"]->getPos());
        buttonList["sound"]->setText((wchar_t *) params_->getMusic().c_str());
        return 0;
    }

    void Option::create_gui(irr::IrrlichtDevice *device) {
        this->setGui(device->getGUIEnvironment());
        irr::gui::IGUISkin* skin = this->getGui()->getSkin();
        irr::gui::IGUIFont* font = this->getGui()->getFont("media/font.png");
        irr::gui::IGUIButton *button;
        irr::gui::IGUIScrollBar* scrollbar;
        irr::gui::IGUIStaticText* text;
        irr::gui::IGUIImage *image;

        if (font)
            skin->setFont(font);
        skin->setFont(this->getGui()->getBuiltInFont(), irr::gui::EGDF_TOOLTIP);
        skin->setColor(irr::gui::EGDC_3D_FACE, irr::video::SColor(255, 201, 65, 178));

        image = this->getGui()->addImage(device->getVideoDriver()->getTexture("media/background_menu"), {0,0});
        imageList["backgroundMenu"] = image;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2,1920/2 - 200,1080/2 + 30),
                                           nullptr, 2,
                                           L"<");
        buttonList["prevSound"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 + 200,1080/2,1920/2 + 300,1080/2 + 30),
                                           nullptr, 2,
                                           L">");
        buttonList["nextSound"] = button;
        button = getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 40,1080/2,1920/2 + 40,1080/2 + 30), nullptr, 2,
                                     (const wchar_t *) params_->getMusic().c_str());
        buttonList["sound"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 + 70,1920/2 + 300,1080/2 + 110),
                                           nullptr, 3,
            L"Return");
        buttonList["Return"] = button;
        text = getGui()->addStaticText(L"Music Volume:", irr::core::rect<irr::s32>(1920/2 - 350,1080/2 - 60,1920/2 + 300,1080/2 - 20), true);
        textList["MusicVolumeText"] = text;
        text = getGui()->addStaticText(L"Musics :", irr::core::rect<irr::s32>(1920/2 - 360,1080/2,1920/2 - 300,1080/2 + 30), true);
        textList["MusicsText"] = text;
        scrollbar = getGui()->addScrollBar(true, irr::core::rect<irr::s32>(1920/2 - 240,1080/2 - 60,1920/2 + 300,1080/2 - 20),
                                           nullptr, 4);
        scrollbar->setMax(100);
        scrollbar->setPos(params_->getMusicVolume());
        scroolList["ScroolMusicVolume"] = scrollbar;
        text = getGui()->addStaticText(L"Effect Volume:", irr::core::rect<irr::s32>(1920/2 - 350,1080/2 - 120,1920/2 + 300,1080/2 - 80), true);
        textList["EffectVolumeText"] = text;
        scrollbar = getGui()->addScrollBar(true, irr::core::rect<irr::s32>(1920/2 - 240,1080/2 - 120,1920/2 + 300,1080/2 - 80),
                                           nullptr, 5);
        scrollbar->setMax(100);
        scrollbar->setPos(params_->getEffectVolume());
        scroolList["ScroolEffectVolume"] = scrollbar;
    }

    void Option::draw() {
        scene_manager_->drawAll();
        std::for_each(this->imageList.begin(), this->imageList.end(),
        [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
            p.second->draw();
        });
        std::for_each(this->buttonList.begin(), this->buttonList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                    p.second->draw();
        });
        std::for_each(this->textList.begin(), this->textList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIStaticText *> &p) {
                    p.second->draw();
        });
        std::for_each(this->scroolList.begin(), this->scroolList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIScrollBar *> &p) {
                    p.second->draw();
        });
    }
}