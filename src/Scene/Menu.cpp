/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Menu
*/

#include "Scene/Menu.hpp"
#include "Core/Core.hpp"

namespace indie
{
    Menu::Menu(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) : AScene(device, s, p) {
        create_gui(device);
    }

    int Menu::manage_event(const EventReceiver &event_receiver) {
        if (buttonList["Play"]->isPressed()) {
            sound_.playSound("select");
            return (sceneAction::addMenu_Play);
        }
        if (buttonList["Option"]->isPressed()) {
            sound_.stopSound("menu2");
            sound_.playSound("select");
            return (sceneAction::addOption);
        }
        if (buttonList["Quit"]->isPressed()) {
            sound_.playSound("confirm");
            return (sceneAction::popScene);
        }
        return 0;
    }

    void Menu::create_gui(irr::IrrlichtDevice *device) {
        this->setGui(device->getGUIEnvironment());
        irr::gui::IGUISkin* skin = this->getGui()->getSkin();
        irr::gui::IGUIFont* font = this->getGui()->getFont("media/font.png");
        irr::gui::IGUIButton *button;
        irr::gui::IGUIImage *image;

        if (font)
            skin->setFont(font);

        image = this->getGui()->addImage(device->getVideoDriver()->getTexture("media/background_menu"), {0,0});
        imageList["backgroundMenu"] = image;
        skin->setFont(this->getGui()->getBuiltInFont(), irr::gui::EGDF_TOOLTIP);
        skin->setColor(irr::gui::EGDC_3D_FACE, irr::video::SColor(200, 201, 65, 178));
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 - 120,1920/2 + 300,1080/2 - 80), 0, 1,
            L"Play");
        buttonList["Play"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 - 60,1920/2 + 300,1080/2 - 20), 0, 2,
            L"Option");
        buttonList["Option"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920/2 - 300,1080/2 ,1920/2 + 300,1080/2 + 40), 0, 3,
            L"Quit");
        buttonList["Quit"] = button;
    }

        void Menu::draw() {
            scene_manager_->drawAll();
            std::for_each(this->imageList.begin(), this->imageList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
                    p.second->draw();
            });
            std::for_each(this->buttonList.begin(), this->buttonList.end(),
                [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                    p.second->draw();
            });
    }
}