/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Menu_Play
*/

#include "Scene/Menu_Play.hpp"
#include "Core/Core.hpp"

namespace indie
{
    Menu_Play::Menu_Play(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) :
            AScene(device, s, p)
    {
        create_gui(device);
    }

    Menu_Play::~Menu_Play()
    {
        std::for_each(this->buttonList.begin(), this->buttonList.end(),
            [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                if (p.second)
                    p.second->remove();
            });
        std::for_each(this->imageList.begin(), this->imageList.end(),
            [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
                if (p.second)
                    p.second->remove();
        });
    }

    int Menu_Play::manage_event(const EventReceiver &event_receiver)
    {
        if (buttonList["One Player"]->isPressed()) {
            sound_.playSound("confirm");
            sound_.stopSound("menu2");
            params_->setDuo(false);
            return (sceneAction::addGame);
        }
        if (buttonList["Two Player"]->isPressed()) {
            sound_.playSound("confirm");
            sound_.stopSound("menu2");
            params_->setDuo(true);
            return (sceneAction::addGame);
        }
        if (buttonList["Return"]->isPressed()) {
            sound_.playSound("select");
            return (sceneAction::popScene);
        }
        return 0;
    }

    void Menu_Play::draw()
    {
        scene_manager_->drawAll();
        std::for_each(this->imageList.begin(), this->imageList.end(),
                [](const std::pair<std::string, irr::gui::IGUIImage *> &p) {
                    p.second->draw();
                });
        std::for_each(this->buttonList.begin(), this->buttonList.end(),
                [](const std::pair<std::string, irr::gui::IGUIButton *> &p) {
                    p.second->draw();
                });
    }

    void Menu_Play::create_gui(irr::IrrlichtDevice *device)
    {
        this->setGui(device->getGUIEnvironment());
        irr::gui::IGUISkin *skin = this->getGui()->getSkin();
        irr::gui::IGUIFont *font = this->getGui()->getFont("../../media/fonthaettenschweiler.bmp");
        irr::gui::IGUIButton *button;
        irr::gui::IGUIImage *image;

        if (font)
            skin->setFont(font);
        skin->setFont(this->getGui()->getBuiltInFont(), irr::gui::EGDF_TOOLTIP);
        skin->setColor(irr::gui::EGDC_3D_FACE, irr::video::SColor(200, 201, 65, 178));
        image = this->getGui()->addImage(device->getVideoDriver()->getTexture("media/background_menu"), {0, 0});
        imageList["backgroundMenu"] = image;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920 / 2 - 300, 1080 / 2 - 120, 1920 / 2, 1080 / 2 - 80), nullptr, 1,
                L"One Player");
        buttonList["One Player"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920 / 2, 1080 / 2 - 120, 1920 / 2 + 300, 1080 / 2 - 80), nullptr, 2,
                L"Two Player");
        buttonList["Two Player"] = button;
        button = this->getGui()->addButton(irr::core::rect<irr::s32>(1920 / 2 - 300, 1080 / 2, 1920 / 2 + 300, 1080 / 2 + 40), nullptr, 3,
                L"Return");
        buttonList["Return"] = button;
    }
}