/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** EventReceiver
*/

#include <iostream>
#include "Events/EventReceiver.hpp"

EventReceiver::EventReceiver()
{
    for (auto &it : key_is_down_)
        it = false;
}

bool EventReceiver::OnEvent(const irr::SEvent &event)
{
    if (event.EventType == irr::EET_KEY_INPUT_EVENT) {
        key_is_down_[event.KeyInput.Key] = event.KeyInput.PressedDown;
        return true;
    }
    return false;
}

bool EventReceiver::IsKeyDown(irr::EKEY_CODE key_code) const
{
    return key_is_down_[key_code];
}
