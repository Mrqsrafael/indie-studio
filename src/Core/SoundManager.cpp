/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** SoundManager
*/

#include "Core/SoundManager.hpp"

namespace indie
{
    void SoundManager::loadSound(std::string const &path, std::string const &name)
    {
        std::shared_ptr<sf::SoundBuffer> buffer = std::make_shared<sf::SoundBuffer>(sf::SoundBuffer());
        std::shared_ptr<sf::Sound> sound = std::make_shared<sf::Sound>(sf::Sound());
        std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>> pair;

        if (!buffer->loadFromFile(path))
            std::cout << "Couldn't load " + path << std::endl;
        sound->setBuffer(*buffer);
        pair.first = buffer;
        pair.second = sound;
        this->_sounds.insert({name, pair});
    }

    void SoundManager::loadMusic(std::string const &path, std::string const &name) {
        std::shared_ptr<sf::SoundBuffer> buffer = std::make_shared<sf::SoundBuffer>(sf::SoundBuffer());
        std::shared_ptr<sf::Sound> sound = std::make_shared<sf::Sound>(sf::Sound());
        std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>> pair;

        if (!buffer->loadFromFile(path))
            std::cout << "Couldn't load " + path << std::endl;
        sound->setBuffer(*buffer);
        pair.first = buffer;
        pair.second = sound;
        this->musics_.insert( {name, pair} );
    }

    void SoundManager::playSound(std::string const &name)
    {
        bool found = false;

        std::for_each(musics_.begin(), musics_.end(),
                      [name, &found](std::pair<const std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> &entry) {
                          if (entry.first == name)
                              found = true;
                      });
        if (found) {
            auto sound = this->musics_.at(name).second;
            sound->play();
        } else {
            auto sound = this->_sounds.at(name).second;
            sound->play();
        }
    }

    void SoundManager::pauseSound(std::string const &name)
    {
        this->_sounds.at(name).second->pause();
    }

    void SoundManager::stopSound(std::string const &name)
    {
        bool found = false;

        std::for_each(musics_.begin(), musics_.end(),
                      [name, &found](std::pair<const std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> &entry) {
                          if (entry.first == name)
                              found = true;
                      });
        if (found)
            this->musics_.at(name).second->stop();
        else
            this->_sounds.at(name).second->stop();
    }

    void SoundManager::resetSound(std::string const &name)
    {
        this->stopSound(name);
        this->playSound(name);
    }

    void SoundManager::enableSoundLoop(std::string const &name)
    {
        bool found = false;

        std::for_each(musics_.begin(), musics_.end(),
                      [name, &found](std::pair<const std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> &entry) {
                          if (entry.first == name)
                              found = true;
                      });
        if (found)
            this->musics_.at(name).second->setLoop(true);
        else
            this->_sounds.at(name).second->setLoop(true);
    }

    void SoundManager::disableSoundLoop(std::string const &name)
    {
        bool found = false;

        std::for_each(musics_.begin(), musics_.end(),
                      [name, &found](std::pair<const std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> &entry) {
                          if (entry.first == name)
                              found = true;
                      });
        if (found)
            this->musics_.at(name).second->setLoop(false);
        else
            this->_sounds.at(name).second->setLoop(false);
    }

    void SoundManager::setSoundVolume(std::string const &name, float volume)
    {
        bool found = false;

        std::for_each(musics_.begin(), musics_.end(),
                      [name, &found](std::pair<const std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> &entry) {
                          if (entry.first == name)
                              found = true;
                      });
        if (found)
            this->musics_.at(name).second->setVolume(volume);
        else
            this->_sounds.at(name).second->setVolume(volume);
    }

    void SoundManager::setMusicSoundVolume(float volume)
    {
        for (auto it : this->musics_)
            it.second.second->setVolume(volume);
    }

    void SoundManager::setEffectSoundVolume(float volume)
    {
        for (auto it : this->_sounds)
            it.second.second->setVolume(volume);
    }

    float SoundManager::getGlobalVolume()
    {
        return sf::Listener::getGlobalVolume();
    }

    void SoundManager::load()
    {
        this->loadMusic("sound/menu.wav", "menu");
        this->loadMusic("sound/menu2.wav", "menu2");
        this->loadMusic("sound/game1.wav", "game1");
        this->loadMusic("sound/game2.wav", "game2");
        this->loadMusic("sound/game3.wav", "game3");
        this->loadMusic("sound/game4.wav", "game4");
        this->loadMusic("sound/win.wav", "win");
        this->loadMusic("sound/loose.wav", "loose");
        this->loadSound("sound/confirm.wav", "confirm");
        this->loadSound("sound/select.wav", "select");
        this->loadSound("sound/PAUSE.wav", "pause");
        this->loadSound("sound/BOMB_SET.wav", "bomb_set");
        this->loadSound("sound/BOMB_EX.wav", "bomb_ex");
        this->loadSound("sound/ITEM_GET.wav", "item_get");
        this->loadSound("sound/PLAYER_OUT.wav", "player_out");
        this->enableSoundLoop("menu2");
        this->enableSoundLoop("game1");
        this->enableSoundLoop("game2");
        this->enableSoundLoop("game3");
        this->enableSoundLoop("game4");
    }

}
