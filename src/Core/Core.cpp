/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Core
*/

#pragma comment(lib, "Ws2_32.lib")

#include "Core/Core.hpp"
#include "MapGenerator.hpp"

namespace indie {
    Core::Core() : receiver_() {
        params_ = std::make_shared<Parameter>();
        sound_.load();
        irr::SIrrlichtCreationParameters params = irr::SIrrlichtCreationParameters();
        params.AntiAlias = 2;
        params.DriverType = irr::video::EDT_SOFTWARE;
        params.WindowSize = irr::core::dimension2d<irr::u32>(1920, 1080);
        params.Fullscreen = 0;
        params.EventReceiver = &receiver_;
        params.HighPrecisionFPU = false;
        params.Vsync = true;
        params.Stencilbuffer = true;
        device_ = irr::createDeviceEx(params);
        device_->setWindowCaption(L"Indie Studio");
        driver_ = device_->getVideoDriver();
        driver_->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS);
        scenes_.push(std::make_unique<Game>(device_, sound_, params_));
    }

    Core::~Core() {
        device_->drop();
    }

    void Core::run() {
        while (device_->run() && driver_) {
            sound_.setMusicSoundVolume(params_->getMusicVolume());
            sound_.setEffectSoundVolume(params_->getEffectVolume());
            int i = 0;
            auto now = std::chrono::system_clock::now();
            std::chrono::duration<float, std::ratio<1, 1000>> elapsed_seconds = now - clock_;
            if (elapsed_seconds.count() > 16) {
                driver_->beginScene(true, true, irr::video::SColor(0, 250, 234, 198));
                i = scenes_.top()->manage_event(receiver_);
                switch (i) {
                    case sceneAction::popScene :
                        scenes_.pop();
                        break;
                    case sceneAction::popGame :
                        scenes_.pop();
                        scenes_.pop();
                        scenes_.pop();
                        break;
                    case sceneAction::Restart :
                        scenes_.pop();
                        scenes_.pop();
                        MapGenerator();
                        scenes_.push(std::make_unique<Game>(device_, sound_, params_));
                        break;
                    case sceneAction::popAll :
                        break;
                    case sceneAction::addMenu :
                        scenes_.push(std::make_unique<Menu>(device_, sound_, params_));
                        break;
                    case sceneAction::addGame :
                        MapGenerator();
                        scenes_.push(std::make_unique<Game>(device_, sound_, params_));
                        break;
                    case sceneAction::addOption :
                        scenes_.push(std::make_unique<Option>(device_, sound_, params_));
                        break;
                    case sceneAction::addMenu_Play :
                        scenes_.push(std::make_unique<Menu_Play>(device_, sound_, params_));
                        break;
                    case sceneAction::winScreen :
                        scenes_.push(std::make_unique<Victory>(device_, sound_, params_));
                        break;
                    case sceneAction::looseScreen :
                        scenes_.push(std::make_unique<Loose>(device_, sound_, params_));
                        break;
                    case sceneAction::addGamePause :
                        scenes_.push(std::make_unique<GamePause>(device_, sound_, params_));
                        break;
                    default:
                        break;
                }
                if (i == sceneAction::popAll)
                    break;
                if (scenes_.empty())
                    return;
                scenes_.top()->draw();
                driver_->endScene();
                clock_ = now;
            }
        }
        while (!scenes_.empty()) {
            scenes_.pop();
        }
    }
}
