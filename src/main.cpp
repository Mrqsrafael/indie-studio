/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** main
*/

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#include "../include/Core/Core.hpp"

int main(void)
{
    indie::Core core;

    core.run();
    return 0;
}