#!/usr/bin/env bash

if (( $# != 1))
then
    echo "You must provide an installation path"
    exit 1
fi

cd build
cmake ..
make
cd ..


mkdir $1
mkdir $1/media
mkdir $1/sound
cp -r media $1/
cp -r sound $1/
cp bomberman $1
