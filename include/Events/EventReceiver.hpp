/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** EventReceiver
*/

#ifndef EVENTRECEIVER_HPP_
#define EVENTRECEIVER_HPP_

#include <irrlicht.h>
#include "driverChoice.h"
#include <array>

class EventReceiver : public irr::IEventReceiver
{
	public:
		EventReceiver();
		~EventReceiver() final = default;

	public:
		bool OnEvent(const irr::SEvent &event) override ;
		bool IsKeyDown(irr::EKEY_CODE key_code) const;

	private:
		std::array<bool, irr::KEY_KEY_CODES_COUNT> key_is_down_;
};

#endif /* !EVENTRECEIVER_HPP_ */
