/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Indie
*/

#ifndef INDIE_HPP_
#define INDIE_HPP_

namespace indie
{
	enum Directions {
			UP = 0,
			RIGHT,
			LEFT,
			DOWN
	};

    enum sceneAction {
        popScene = 1,
        popGame,
        popAll,
        addGame,
        addOption,
        addMenu,
        addMenu_Play,
        winScreen,
        looseScreen,
        addGamePause,
        Restart
    };
}

#endif /* !INDIE_HPP_ */
