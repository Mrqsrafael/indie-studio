//
// Created by mrafael on 6/11/19.
//

#ifndef BOMBERMAN_POWERUP_HPP
#define BOMBERMAN_POWERUP_HPP

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IMesh.h"
#include <map>

namespace indie
{
    class PowerUp
    {
    public:
        PowerUp(irr::scene::ISceneNode *parent,
            irr::scene::ISceneManager *mgr, irr::s32 id = -1,
            const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0),
            const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0),
            const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
        {
            mesh_ = mgr->getMesh("media/Crate1.obj");
            if (mesh_) {
                node_ = mgr->addOctreeSceneNode(mesh_, parent, id);
            }
            name_ = tpower_up_[std::rand() % 3];
            if (node_) {
                node_->setPosition(position);
                node_->setRotation(rotation);
                node_->setScale(scale);
                node_->setMaterialTexture(0, mgr->getVideoDriver()->getTexture(name_.c_str()));
                node_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
            }
        }

        ~PowerUp() {
            if (node_)
                node_->remove();
        }

        irr::core::vector3df getPosition() { return node_->getPosition(); }

        std::string getName() { return name_; }

        std::map<int, std::string> getMap() { return tpower_up_; }

        void destroy() {
            node_->remove();
            node_ = nullptr;
        }

    private:

    private:
        irr::scene::IMesh *mesh_ = nullptr;
        irr::scene::IMeshSceneNode *node_ = nullptr;
        std::map<int, std::string> tpower_up_ = {
                {0, "media/Bomb_Up.png"},
                {1, "media/Skate.png"},
                {2, "media/Fire.png"}
        };
        std::string name_;
    };
}

#endif //BOMBERMAN_POWERUP_HPP
