/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Bomb
*/

#ifndef BOMB_HPP_
#define BOMB_HPP_

#include <ISceneNode.h>
#include "Indie.hpp"

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "irrlicht.h"

#include <ITimer.h>
#include <ctime>
#include <chrono>
#include <array>
#include <iostream>

#define EXPLODE_TIMER 1.5
#define PARTICLE_TIMER 0.3

namespace indie
{
    class Bomb
	{
		public:
            Bomb(irr::scene::ISceneNode *parent,
                   irr::scene::ISceneManager *mgr, irr::s32 id = -1,
                   const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0),
                   const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0),
                   const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
            {
                mesh_ = mgr->getMesh("media/model_ob001_bomb.dae");
                if (mesh_) {
                    node_ = mgr->addOctreeSceneNode(mesh_, parent, id);
                }
                if (node_) {
                    node_->setPosition(position);
                    node_->setRotation(rotation);
                    node_->setScale(scale);
                    node_->setMaterialTexture(0, mgr->getVideoDriver()->getTexture("media/tex_ob001_bomb_AL.png"));
                    node_->setMaterialFlag(irr::video::EMF_LIGHTING, true);
                }
                for (std::size_t i = 0; i < particleSystem_.max_size(); i++) {
                    particleSystem_[i] = mgr->addParticleSystemSceneNode(false);
                    emitter_[i] = nullptr;
                }
                launch();
            }

            ~Bomb() {
                for (auto it : emitter_) {
                    if (it) {
                        it->setMaxParticlesPerSecond(0);
                        it->drop();
                    }
                }
                if (node_)
                    node_->remove();
            }

            bool checkTimer() {
                auto now = std::chrono::system_clock::now();
                std::chrono::duration<float> elapsed_seconds = now - clock_;


                return elapsed_seconds.count() > EXPLODE_TIMER;
            }

            bool checkParticle() {
                auto now = std::chrono::system_clock::now();
                std::chrono::duration<float> elapsed_seconds = now - clock_;

                return elapsed_seconds.count() > PARTICLE_TIMER;
            }

            void doParticles(std::array<int, 4>  hitbox) {
                directions_[0] = irr::core::vector3df(-hitbox[0]/(hitbox[0] >= 6 ? 3 : 2), 1.0f, 0.0f);
                directions_[1] = irr::core::vector3df(hitbox[1]/(hitbox[1] >= 6 ? 3 : 2), 1.0f, 0.0f);
                directions_[2] = irr::core::vector3df(0.0f, 1.0f, -hitbox[2]/(hitbox[2] >= 6 ? 3 : 2));
                directions_[3] = irr::core::vector3df(0.0f, 1.0f, hitbox[3]/(hitbox[3] >= 6 ? 3 : 2));
                for (int i = 0; i < emitter_.max_size(); i++) {
                    emitter_[i] = particleSystem_[i]->createCylinderEmitter (
                            irr::core::vector3df(node_->getPosition()),
                            irr::f32(1),
                            directions_[i],
                            irr::f32(hitbox[i]/(hitbox[i] > 6 ? 3 : 2)),
                            false,
                            irr::core::vector3df(0.0f,0.0f,0.0f),
                            300,300,
                            irr::video::SColor(0,128,0,0),
                            irr::video::SColor(0,246,199,0),
                            60, 100,
                            0,
                            irr::core::dimension2df(0.1f,0.1f),
                            irr::core::dimension2df(1.0f,1.0f));
                    particleSystem_[i]->setEmitter(emitter_[i]);
                    particleSystem_[i]->setMaterialFlag(irr::video::EMF_LIGHTING, false);

                }
                node_->remove();
                node_ = nullptr;
                clock_ = std::chrono::system_clock::now();
                this->used = true;
            }

            const irr::core::vector3df& explode() {
                return node_->getPosition();
            }

	    public:
            bool used = false;

    private:
	        void launch() { clock_ = std::chrono::system_clock::now(); }

            std::chrono::system_clock::time_point clock_;
            irr::scene::IMesh *mesh_ = nullptr;
            irr::scene::IMeshSceneNode *node_ = nullptr;
            std::array<irr::scene::IParticleSystemSceneNode *, 4> particleSystem_;
            std::array<irr::scene::IParticleEmitter *, 4> emitter_;
            std::array<irr::core::vector3df , 4>  directions_;
    };
}


#endif /* !BOMB_HPP_ */
