#ifndef BOMBERMAN_WALL_HPP
#define BOMBERMAN_WALL_HPP

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IMesh.h"
#include "ITriangleSelector.h"
#include "ABox.hpp"

namespace indie
{
    class Wall : public ABox
    {
        public:
            Wall(irr::scene::ISceneNode *parent,
                    irr::scene::ISceneManager *mgr, irr::s32 id = 1,
                    const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
                    :   ABox(parent, mgr, id, position, rotation, scale)
            {
                if (node_) {
                    node_->setMaterialTexture(0, mgr->getVideoDriver()->getTexture("media/iron_box.jpg"));
                    node_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
                }
            }

            ~Wall() override {}
    };
}


#endif //BOMBERMAN_WALL_HPP
