/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** DestructibleBox
*/

#ifndef BOX_HPP_
#define BOX_HPP_

#include <vector>
#include <memory>
#include <iostream>

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IMesh.h"
#include "ITriangleSelector.h"
#include "ABox.hpp"

namespace indie
{
    class DestructibleBox : public ABox
    {
        public:
            DestructibleBox(irr::scene::ISceneNode *parent,
                    irr::scene::ISceneManager *mgr, irr::s32 id = 1,
                    const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
                    :   ABox(parent, mgr, id, position, rotation, scale)
            {
                if (node_) {
                    node_->setMaterialTexture(0, mgr->getVideoDriver()->getTexture("media/crate_1.jpg"));
                    node_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
                }
            }

            ~DestructibleBox() override
            {
//                if (node_)
//                    node_->remove();
            }

        public:
            void destroy()
            {
                node_->remove();
                node_ = nullptr;
            }

        public:
            auto getPosition()
            {
                return node_->getPosition();
            }

    };
}

#endif /* !BOX_HPP_ */
