#ifndef BOMBERMAN_ABOX_HPP
#define BOMBERMAN_ABOX_HPP

#include <iostream>

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IMesh.h"
#include "ITriangleSelector.h"

namespace indie
{
    class ABox
    {
        public:
            ABox(irr::scene::ISceneNode *parent, irr::scene::ISceneManager *mgr,
                    irr::s32 id,
                    const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
                    :   smgr_(mgr),
                        mesh_(mgr->getMesh("media/Crate1.obj"))
            {
                if (mesh_)
                    node_ = mgr->addOctreeSceneNode(mesh_->getMesh(0), parent, 1);
                if (node_) {
                    node_->setPosition(position);
                    node_->setRotation(rotation);
                    node_->setScale(scale);
                    irr::scene::ITriangleSelector *selector = smgr_->createOctreeTriangleSelector(node_->getMesh(), node_);
                    if (selector) {
                        node_->setTriangleSelector(selector);
                        selector->drop();
                    }
                }
            }

            virtual ~ABox()
            {
                if (node_) {
                    node_->remove();
                }
            }

        public:
            auto getNode()
            {
                return node_;
            }

        protected:
            irr::scene::ISceneManager *smgr_ = nullptr;
            irr::scene::IMeshSceneNode *node_ = nullptr;
            irr::scene::IAnimatedMesh *mesh_ = nullptr;
    };
}

#endif //BOMBERMAN_ABOX_HPP
