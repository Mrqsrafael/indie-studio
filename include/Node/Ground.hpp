#ifndef BOMBERMAN_GROUND_HPP
#define BOMBERMAN_GROUND_HPP

#include <iostream>
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IMeshSceneNode.h"

namespace indie
{
    class Ground
    {
        public:
            Ground(irr::scene::ISceneNode* parent,irr::scene::ISceneManager *mgr,
                    const irr::core::vector3df& position = irr::core::vector3df(0,0,0),
                    const irr::core::vector3df& rotation = irr::core::vector3df(0,0,0),
                    const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
                    :   node_(mgr->addCubeSceneNode(2.f, parent, -1, position))
            {
                node_->setMaterialTexture(0, mgr->getVideoDriver()->getTexture("media/Ground.jpg"));
                node_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
            }

            ~Ground()
            {
                if (node_)
                    node_->remove();
            }

        private:
            irr::scene::IMeshSceneNode *node_ = nullptr;
    };
}

#endif //BOMBERMAN_GROUND_HPP
