//
// Created by mrafael on 6/13/19.
//

#ifndef BOMBERMAN_BOMBMENU_HPP
#define BOMBERMAN_BOMBMENU_HPP

#define EXPLO_TIMER 2.5
#define PARTCLE_TIMER 0.5

#include <ISceneNode.h>
#include "Indie.hpp"

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "irrlicht.h"

#include <ITimer.h>
#include <ctime>
#include <chrono>
#include <array>

namespace indie
{
    class BombM
    {
        public:
            BombM(irr::scene::ISceneNode *parent,
                    irr::scene::ISceneManager *mgr, irr::s32 id = -1,
                    const irr::core::vector3df &position = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &rotation = irr::core::vector3df(0, 0, 0),
                    const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f))
            {
                mesh_ = mgr->getMesh("media/model_ob001_bomb.dae");
                if (mesh_) {
                    node_ = mgr->addOctreeSceneNode(mesh_, parent, id);
                }
                if (node_) {
                    node_->setPosition(position);
                    node_->setRotation(rotation);
                    node_->setScale(scale);
                    node_->setMaterialTexture(0, mgr->getVideoDriver()->getTexture("media/tex_ob001_bomb_AL.png"));
                    node_->setMaterialFlag(irr::video::EMF_LIGHTING, true);
                }
                particleSystem_ = mgr->addParticleSystemSceneNode(false);
            }

            ~BombM()
            {
                if (node_) {
                    node_->remove();
                    node_ = nullptr;
                }
                if (emitter_) {
                    emitter_->setMaxParticlesPerSecond(0);
                    emitter_->drop();
                }
            }

        public:
            void check_fall(std::chrono::system_clock::time_point now)
            {
                std::chrono::duration<float> elapsed_move = now - clock_2;
                if (node_ != nullptr) {
                    irr::core::vector3df next_pos = node_->getPosition();
                    next_pos.Y -= 2;
                    if (elapsed_move.count() > 0.1) {
                        node_->setPosition(next_pos);
                        clock_2 = now;
                    }
                }
            }

            bool checkTimer()
            {
                auto now = std::chrono::system_clock::now();
                std::chrono::duration<float> elapsed_seconds = now - clock_;
                check_fall(now);

                return elapsed_seconds.count() > EXPLO_TIMER;
            }

            bool checkParticle()
            {
                auto now = std::chrono::system_clock::now();
                std::chrono::duration<float> elapsed_seconds = now - clock_;

                return elapsed_seconds.count() > PARTCLE_TIMER;
            }

            void doParticles()
            {
                emitter_ = particleSystem_->createSphereEmitter(
                        irr::core::vector3df(node_->getPosition()),
                        irr::f32(3),
                        irr::core::vector3df(0.0f, 0.0f, 0.0f),
                        200, 200,
                        irr::video::SColor(0, 128, 0, 0),
                        irr::video::SColor(0, 246, 199, 0),
                        60, 100,
                        0,
                        irr::core::dimension2df(3.5f, 3.5f),
                        irr::core::dimension2df(4.5f, 4.5f));
                particleSystem_->setEmitter(emitter_);
                particleSystem_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
                node_->remove();
                node_ = nullptr;
                clock_ = std::chrono::system_clock::now();
                this->used = true;
            }

        public:
            bool used = false;

        private:
            std::chrono::system_clock::time_point clock_ = std::chrono::system_clock::now();
            std::chrono::system_clock::time_point clock_2 = std::chrono::system_clock::now();
            irr::scene::IMesh *mesh_ = nullptr;
            irr::scene::IMeshSceneNode *node_ = nullptr;
            irr::scene::IParticleSystemSceneNode *particleSystem_ = nullptr;
            irr::scene::IParticleEmitter *emitter_ = nullptr;
    };
}

#endif //BOMBERMAN_BOMBMENU_HPP
