/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Map
*/

#ifndef MAPNODE_HPP_
#define MAPNODE_HPP_

#include <string>
#include <map>
#include <fstream>
#include <vector>

#include "ISceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IAnimatedMeshSceneNode.h"
#include "Node/Box/DestructibleBox.hpp"
#include "Node/Item/PowerUP.hpp"
#include "Node/Item/Bomb.hpp"
#include "Node/Box/Wall.hpp"
#include "Character/Player.hpp"
#include "Character/IA.hpp"
#include "Ground.hpp"
#include "Core/SoundManager.hpp"
#include "Parameter.hpp"

namespace indie
{
    class Map
    {
        public:
            Map(irr::scene::ISceneNode *parent,
                    irr::scene::ISceneManager *mgr, std::shared_ptr<Parameter> params, irr::s32 id = -1)
                    :   smgr_(mgr),
                        node_(mgr->addEmptySceneNode(parent, id)),
                        meta_box_selector_(mgr->createMetaTriangleSelector())
            {
                createMap_(params);
                irr::scene::ISceneNodeAnimatorCollisionResponse *anim = nullptr;
                irr::core::aabbox3d<irr::f32> bbox;
                irr::core::vector3df radius;
                if (meta_box_selector_) {
                    for (auto &box : boxes_)
                        meta_box_selector_->addTriangleSelector(box->getNode()->getTriangleSelector());
                    for (auto &wall : walls_)
                        meta_box_selector_->addTriangleSelector(wall->getNode()->getTriangleSelector());
                    for (auto &player : player_) {
                        bbox = player->getNode()->getBoundingBox();
                        radius = bbox.MaxEdge, bbox.getCenter();
                        anim = smgr_->createCollisionResponseAnimator(meta_box_selector_, player->getNode(),
                                radius, irr::core::vector3df(0, 0, 0));
                        player->getNode()->addAnimator(anim);
                        anim->drop();
                    }
                }
            }

            Map(const Map &) = delete;

            Map &operator=(const Map &) = delete;

            ~Map() = default;

        public:
        void checkMap(SoundManager &sound) {
            for (auto p  = player_.rbegin(); p != player_.rend(); p++) {
                for (auto pow = PowerUps_.rbegin(); pow != PowerUps_.rend(); pow++) {
                        if ((*pow)->getPosition().X == getCenter((*p)->getPlayerPosition()).X && (*pow)->getPosition().Z == getCenter((*p)->getPlayerPosition()).Z) {
                            if ((*pow)->getName() == (*pow)->getMap()[0]) {
                                (*p)->LevelUpMaxBomb();
                                sound.playSound("item_get");
                            }
                            if ((*pow)->getName() == (*pow)->getMap()[1]) {
                                (*p)->LevelUpMaxSpeed();
                                sound.playSound("item_get");
                            }
                            if ((*pow)->getName() == (*pow)->getMap()[2]) {
                                (*p)->LevelUpRange();
                                sound.playSound("item_get");
                            }
                            (*pow)->destroy();
                            std::advance(pow, 1);
                            PowerUps_.erase(pow.base());
                            std::advance(pow, -1);
                        }
                    }
                }
        }

            void isBlock(irr::core::vector3df bomb, std::array<int, 4> &directions, int distance,
                    std::array<int, 4> &partciles)
            {
                int y = bomb.X / 2;
                int x = bomb.Z / 2;
                int to_increment = distance == 1 ? 0 : distance;

                if (y - distance >= 0 && map_[y - distance][x] == 'X' && directions[0] != 0) {
                    directions[0] = to_increment * 2;
                    partciles[0] = to_increment * 2;
                }
                if (y + distance <= 12 && map_[y + distance][x] == 'X' && directions[1] != 0) {
                    directions[1] = to_increment * 2;
                    partciles[1] = to_increment * 2;
                }
                if (x - distance >= 0 && map_[y][x - distance] == 'X' && directions[2] != 0) {
                    directions[2] = to_increment * 2;
                    partciles[2] = to_increment * 2;
                }
                if (x + distance <= 15 && map_[y][x + distance] == 'X' && directions[3] != 0) {
                    directions[3] = to_increment * 2;
                    partciles[3] = to_increment * 2;
                }
            }

            std::array<int, 4> is_player(irr::core::vector3df bomb, std::array<int, 4> &directions) {
                    std::array<int, 4> array = {0,0,0,0};
                    int i = 3;
                    for (auto p = player_.rbegin(); p != player_.rend(); p++) {
                        if (bomb.X - directions[0] <= getCenter((*p)->getPlayerPosition()).X
                        && bomb.X + directions[1] >= getCenter((*p)->getPlayerPosition()).X
                        && bomb.Z == getCenter((*p)->getPlayerPosition()).Z && !(*p)->getDead()) {
                            (*p)->addKill(1);
                            array[i] = 1;
                        } else if ((bomb.Z - directions[2] <= getCenter((*p)->getPlayerPosition()).Z
                        && bomb.Z + directions[3] >= getCenter((*p)->getPlayerPosition()).Z)
                        && bomb.X == getCenter((*p)->getPlayerPosition()).X && !(*p)->getDead()) {
                            array[i] = 1;
                        }
                        i--;
                    }
                    return array;
            }

            static irr::core::vector3df getCenter(irr::core::vector3df bomb)
            {
                bomb.X += 1;
                bomb.Z += 1;
                int x = bomb.X;
                int y = bomb.Z;

                x = ((x % 2) ? bomb.X - 1 : bomb.X);
                y = ((y % 2) ? bomb.Z - 1 : bomb.Z);
                return irr::core::vector3df(x, -1, y);
            }

            std::array<int, 4> destroy(irr::core::vector3df bomb, int radius)
            {
                std::array<int, 4> directions = {2, 2, 2, 2};
                std::array<int, 4> particles = {2, 2, 2, 2};

                map_[bomb.X/2][bomb.Z/2] = ' ';
                for (int i = 0; i < radius; i++) {
                    isBlock(bomb, directions, i, particles);
                    for (auto box = boxes_.rbegin(); box != boxes_.rend(); ++box) {
                        if ((*box)->getPosition().X == (bomb.X - directions[0]) && (*box)->getPosition().Z == bomb.Z) {
                            meta_box_selector_->removeTriangleSelector((*box)->getNode()->getTriangleSelector());
                            eraseComponent_(box);
                            directions[0] = 0;
                        } else if ((*box)->getPosition().X == (bomb.X + directions[1]) && (*box)->getPosition().Z == bomb.Z) {
                            meta_box_selector_->removeTriangleSelector((*box)->getNode()->getTriangleSelector());
                            eraseComponent_(box);
                            directions[1] = 0;
                        } else if ((*box)->getPosition().Z == (bomb.Z - directions[2]) && (*box)->getPosition().X == bomb.X) {
                            meta_box_selector_->removeTriangleSelector((*box)->getNode()->getTriangleSelector());
                            eraseComponent_(box);
                            directions[2] = 0;
                        } else if ((*box)->getPosition().Z == (bomb.Z + directions[3]) && (*box)->getPosition().X == bomb.X) {
                            meta_box_selector_->removeTriangleSelector((*box)->getNode()->getTriangleSelector());
                            eraseComponent_(box);
                            directions[3] = 0;
                        }
                    }
                    for (int n = 0; n < directions.max_size() && i != radius - 1; n++)
                        if (directions[n] != 0) {
                            directions[n] += 2;
                            particles[n] += 2;
                        }
                }
                return particles;
            }


        private:
            void eraseComponent_(std::vector<std::unique_ptr<DestructibleBox>>::reverse_iterator &box)
            {
                if (std::rand() % 10 < 3) {
                    PowerUps_.push_back(std::make_unique<PowerUp>(node_, smgr_, 1, (*box)->getPosition(),
                            irr::core::vector3df(0, 0, 0),
                            irr::core::vector3df(0.5f, 0.5f, 0.5f)));
                }
                map_[(*box)->getPosition().X/2][(*box)->getPosition().Z/2] = ' ';
                (*box)->destroy();
                std::advance(box, 1);
                boxes_.erase(box.base());
                std::advance(box, -1);
            }

            void createMap_(std::shared_ptr<Parameter> params)
            {
                float x = 0;
                float y = 0;
                float z = 0;

                fillData_();
                for (auto &i : map_) {
                    for (auto &n : i) {
                        grounds_.push_back(std::make_unique<Ground>(node_, smgr_, irr::core::vector3df(x, -2, z)));
                        if (n == 'P')
                            addPlayer_(irr::core::vector3df(x, -1, z), params);
                        else if (n == '*')
                            boxes_.push_back(std::make_unique<DestructibleBox>(node_, smgr_, 1, irr::core::vector3df(x, y, z)));
                        else if (n != ' ')
                            walls_.push_back(std::make_unique<Wall>(node_, smgr_, -1, irr::core::vector3df(x, y, z)));
                        z += 2;
                    }
                    x += 2;
                    z = 0;
                }
            }

            void addPlayer_(const irr::core::vector3df &pos, std::shared_ptr<Parameter> params)
            {
                static int nb = 0;
                irr::scene::IAnimatedMeshSceneNode *node;

                if (nb == 0)
                    player_.emplace_back(std::make_shared<indie::Player>(smgr_->getRootSceneNode(), smgr_, tplayer_[nb], params->getMap1(), pos, irr::core::vector3df(0, -90, 0)));
                else if (nb == 1)
                    if (params->getDuo())
                        player_.emplace_back(std::make_shared<indie::Player>(smgr_->getRootSceneNode(), smgr_, tplayer_[nb], params->getMap2(), pos, irr::core::vector3df(0, -90, 0)));
                    else
                        player_.emplace_back(std::make_shared<indie::IA>(smgr_->getRootSceneNode(), smgr_, tplayer_[nb], pos, irr::core::vector3df(0, -90, 0)));
                else if (nb == 2)
                    player_.emplace_back(std::make_shared<indie::IA>(smgr_->getRootSceneNode(), smgr_, tplayer_[nb], pos, irr::core::vector3df(0, 90, 0)));
                else if (nb == 3) {
                    player_.emplace_back(
                            std::make_shared<indie::IA>(smgr_->getRootSceneNode(), smgr_, tplayer_[nb], pos,
                                                        irr::core::vector3df(0, 90, 0)));
                    nb = -1;
                }
                nb++;
            }

            void fillData_()
            {
                std::ifstream mapFile("map.txt");
                std::string string;

                if (mapFile.is_open()) {
                    while (!mapFile.eof()) {
                        std::getline(mapFile, string);
                        if (!string.empty())
                            map_.push_back(string);
                    }
                    mapFile.close();
                }
            }

        public:
            std::vector<std::shared_ptr<indie::ACharacter>> &getPayers()
            {
                return this->player_;
            }

            std::vector<std::string> &getMap() { return map_; }

        private:
            irr::scene::ISceneNode *node_ = nullptr;
            irr::scene::ISceneManager *smgr_ = nullptr;
            irr::scene::IMetaTriangleSelector *meta_box_selector_ = nullptr;
            std::vector<std::unique_ptr<Ground>> grounds_;
            std::vector<std::unique_ptr<DestructibleBox>> boxes_;
            std::vector<std::unique_ptr<Wall>> walls_;
            std::vector<std::unique_ptr<PowerUp>> PowerUps_;
            std::map<int, std::string> tplayer_ = {
                    {0, "media/bomberman_blue.png"},
                    {1, "media/bomberman_green.png"},
                    {2, "media/bomberman_black.png"},
                    {3, "media/bomberman_pink.png"}
            };
            std::vector<std::string> map_;
            std::vector<std::shared_ptr<indie::ACharacter>> player_;
    };
}

#endif /* !MAPNODE_HPP_ */
