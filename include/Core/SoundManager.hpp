/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** SoundManager
*/

#ifndef SOUNDMANAGER_HPP
#define SOUNDMANAGER_HPP

#include <memory>
#include <unordered_map>
#include <iostream>
#include "SFML/Audio.hpp"

namespace indie {
    class SoundManager {

    private:
        std::unordered_map<std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> musics_;
        std::unordered_map<std::string, std::pair<std::shared_ptr<sf::SoundBuffer>, std::shared_ptr<sf::Sound>>> _sounds;


    public:
        SoundManager() = default;
        ~SoundManager() = default;

        void load();
        void setMusicSoundVolume(float volume);
        void setEffectSoundVolume(float volume);
        void loadSound(std::string const &path, std::string const &name);
        void loadMusic(std::string const &path, std::string const &name);
        void playSound(std::string const &name);
        void pauseSound(std::string const &name);
        void stopSound(std::string const &name);
        void resetSound(std::string const &name);
        void enableSoundLoop(std::string const &name);
        void disableSoundLoop(std::string const &name);
        void setSoundVolume(std::string const &name, float volume);
        float getGlobalVolume();
    };
}

#endif //SOUNDMANAGER_HPP
