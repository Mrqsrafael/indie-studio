/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Core
*/

#ifndef CORE_HPP_
#define CORE_HPP_

#include <stack>
#include <memory>
#include <algorithm>
#include <ctime>
#include <chrono>

#include "Core/ICore.hpp"
#include "Scene/Game.hpp"
#include "Scene/AScene.hpp"
#include "Scene/Menu.hpp"
#include "Scene/Option.hpp"
#include "Scene/Menu_Play.hpp"
#include "Scene/Introduction.hpp"
#include "Scene/Victory.hpp"
#include "Scene/Loose.hpp"
#include "Scene/GamePause.hpp"
#include "../Parameter.hpp"
#include "Indie.hpp"

namespace indie {
    class Core : public ICore {
    public:
        Core();

        ~Core() final;


        void run();

    private:
        std::chrono::system_clock::time_point clock_ = std::chrono::system_clock::now();
        EventReceiver receiver_;
        std::stack<std::unique_ptr<AScene>> scenes_;
        irr::IrrlichtDevice *device_;
        irr::video::IVideoDriver *driver_;
        std::shared_ptr<Parameter> params_;
        SoundManager sound_;
    };
}

#endif /* !CORE_HPP_ */
