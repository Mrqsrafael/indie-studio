/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** ICore
*/

#ifndef ICORE_HPP_
#define ICORE_HPP_

#include "Indie.hpp"

namespace indie {
    class ICore {
    	public:
            virtual ~ICore() {}
    };
}

#endif /* !ICORE_HPP_ */
