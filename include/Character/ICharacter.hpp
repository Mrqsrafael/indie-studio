/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** ICharacter
*/

#ifndef ICHARACTER_HPP_
#define ICHARACTER_HPP_

#include "Indie.hpp"

namespace indie
{
	class ICharacter
	{
		public:
			virtual ~ICharacter() = default;

			virtual void move(float new_rot, Directions direction) = 0;
            virtual void standUp() = 0;
            virtual bool addBomb(irr::scene::ISceneNode *parent,
                     irr::scene::ISceneManager *mgr, irr::s32 id,
                     const irr::core::vector3df &position) = 0;
            virtual void LevelUpMaxBomb() = 0;
            virtual void LevelUpMaxSpeed() = 0;
            virtual void LevelUpRange() = 0;
            virtual bool brain(std::vector<std::string> map) = 0;

        // getter

            virtual std::map<std::string, irr::EKEY_CODE> *getMap() = 0;
            virtual int getMaxBomb() = 0;
            virtual float getSpeed() = 0;
            virtual int getRange() = 0;
            virtual int getKill() = 0;
            virtual unsigned int getRank() = 0;
            virtual bool getDead() = 0;
            virtual irr::core::vector3df getPlayerPosition() = 0;
            virtual irr::core::vector3df getPlayerRotation() = 0;
            virtual std::vector<std::unique_ptr<Bomb>> &getBombs() = 0;
            virtual irr::scene::IAnimatedMeshSceneNode *getNode() = 0;

            // setter

            virtual void addKill(int kill) = 0;
            virtual void setAnimation(irr::scene::IAnimatedMesh *mesh,
                                      const irr::core::vector3df &pos, const irr::core::vector3df &rot) = 0;
            virtual void setRank(unsigned int rank) = 0;
            virtual void setPlayerPosition(irr::core::vector3df &new_pos) = 0;
            virtual void setRotation(irr::core::vector3df &new_rot) = 0;
            virtual void setDead(bool deadStatus) = 0;
	};
}

#endif /* !ICHARACTER_HPP_ */
