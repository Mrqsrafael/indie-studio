/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** ACharacter
*/

#ifndef ABSTRACTCHARACTER_HPP_
#define ABSTRACTCHARACTER_HPP_

#include <memory>
#include <map>
#include <vector>
#include "IMeshSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "IAnimatedMeshSceneNode.h"

#include "Node/Item/Bomb.hpp"
#include "Indie.hpp"
#include "ISceneNode.h"
#include "ICharacter.hpp"

namespace indie
{
	class ACharacter : public ICharacter
	{
		public:
			ACharacter(irr::scene::ISceneNode *parent, irr::scene::ISceneManager *smgr,
                        const std::string &texture,
                        const irr::core::vector3df &pos = irr::core::vector3df(0,0,0),
                        const irr::core::vector3df &rot = irr::core::vector3df(0,0,0),
                        const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f),
                        bool alsoAddIfMeshPointerZero=false)
                        :   SceneManager_(smgr),
                            stand_mesh_(SceneManager_->getMesh("media/bomberman_stand.b3d")),
                            run_mesh_(SceneManager_->getMesh("media/bomberman_run.b3d")),
                            node_(smgr->addAnimatedMeshSceneNode(stand_mesh_, parent, 1,
                                    pos, rot, scale, alsoAddIfMeshPointerZero)),
                         texture_(texture)
            {
                node_->setMaterialTexture(0, SceneManager_->getVideoDriver()->getTexture(texture.c_str()));
                node_->setMaterialFlag(irr::video::EMF_LIGHTING, false);
                irr::scene::ITriangleSelector *selector = smgr->createTriangleSelector(node_);
                node_->setTriangleSelector(selector);
                selector->drop();
            }

			~ACharacter() override {
			    if (node_) {
                    node_->remove();
                    node_ = nullptr;
                }
			}

        public:
            void move(float new_rot, Directions direction) final;
            bool addBomb(irr::scene::ISceneNode *parent,
                irr::scene::ISceneManager *mgr, irr::s32 id,
                const irr::core::vector3df &position) final;
            void setAnimation(irr::scene::IAnimatedMesh *mesh, const irr::core::vector3df &pos, const irr::core::vector3df &rot) final;
            void standUp() final;
            void LevelUpMaxBomb() final;
            void LevelUpMaxSpeed() final;
            void LevelUpRange() final;
            bool brain(std::vector<std::string> map) override { return false; }

        //  Getter

            std::map<std::string, irr::EKEY_CODE> *getMap() override { return nullptr; }

            int getMaxBomb() final { return MaxBomb_; }

            float getSpeed() final { return MaxSpeed_; }

            int getRange() final { return Range_; }

            int getKill() final { return kill_; }

            unsigned int getRank() final { return this->rank_; }

            bool getDead() final { return this->isDead; }

            irr::core::vector3df getPlayerPosition()  final { return node_->getPosition(); }

            irr::core::vector3df getPlayerRotation() final { return node_->getRotation(); }

            std::vector<std::unique_ptr<Bomb>> &getBombs() final { return bombs_; }

            irr::scene::IAnimatedMeshSceneNode *getNode() final { return node_; }

        //  Setter

            void addKill(int kill) final { kill_ += kill; }

            void setRank(unsigned int rank) final { rank_ = rank; }

            void setPlayerPosition(irr::core::vector3df &new_pos) final { node_->setPosition(new_pos); }

            void setRotation(irr::core::vector3df &new_rot) final { node_->setRotation(new_rot); }

            void setDead(bool deadStatus) final { this->isDead = deadStatus; }

		protected:
            irr::scene::ISceneManager *SceneManager_ = nullptr;
            irr::scene::IAnimatedMesh *stand_mesh_ = nullptr;
            irr::scene::IAnimatedMesh *run_mesh_ = nullptr;
            irr::scene::IAnimatedMeshSceneNode *node_ = nullptr;
            irr::scene::ITriangleSelector *triangle_selector_ = nullptr;
            std::string texture_ = nullptr;
            std::vector<std::unique_ptr<Bomb>> bombs_;
            int MaxBomb_ = 1;
            float MaxSpeed_ = 0.22;
            int Range_ = 2;
            bool isDead = false;
            unsigned int kill_ = 0;
            unsigned int rank_ = 0;	};
}

#endif /* !ABSTRACTCHARACTER_HPP_ */
