/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Player
*/

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include <map>

#include "ACharacter.hpp"

namespace indie {
    class Player : public ACharacter {
    public:
        Player(irr::scene::ISceneNode *parent, irr::scene::ISceneManager *smgr,
               const std::string &texture,
               std::map<std::string, irr::EKEY_CODE> keymap,
               const irr::core::vector3df &pos = irr::core::vector3df(0, 0, 0),
               const irr::core::vector3df &rot = irr::core::vector3df(0, 0, 0),
               const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f),
               bool alsoAddIfMeshPointerZero = false)
                : ACharacter(parent, smgr, texture, pos, rot, scale, alsoAddIfMeshPointerZero) {
            this->keymap = keymap;
        }

        ~Player() override = default;

        Player(const Player &) = delete;

        Player &operator=(const Player &) = delete;

    public:
        std::map<std::string, irr::EKEY_CODE> *getMap() final { return &this->keymap; }

    public:
        std::map<std::string, irr::EKEY_CODE> keymap = {
                {"Up",    irr::KEY_KEY_Z},
                {"Down",  irr::KEY_KEY_S},
                {"Left",  irr::KEY_KEY_Q},
                {"Right", irr::KEY_KEY_D},
                {"Bomb",  irr::KEY_SPACE},
        };
    };
}

#endif /* !PLAYER_HPP_ */
