/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** IA
*/

#ifndef IA_HPP_
#define IA_HPP_

#include "Indie.hpp"
#include "ACharacter.hpp"

namespace indie
{
    class IA : public ACharacter
    {
    	public:
            IA(irr::scene::ISceneNode *parent, irr::scene::ISceneManager *smgr,
            const std::string &texture,
            const irr::core::vector3df &pos = irr::core::vector3df(0,0,0),
            const irr::core::vector3df &rot = irr::core::vector3df(0,0,0),
            const irr::core::vector3df &scale = irr::core::vector3df(1.0f, 1.0f, 1.0f),
            bool alsoAddIfMeshPointerZero=false)
            :   ACharacter(parent, smgr, texture, pos, rot, scale, alsoAddIfMeshPointerZero)
            {
                std::srand(std::time(nullptr));
            }

            ~IA() override = default;
            IA(const IA&) = delete;
            IA& operator=(const IA&) = delete;

            void hide(std::vector<std::string> map, int hide, Directions dir);

            bool checkTimer(float t) {
                auto now = std::chrono::system_clock::now();
                std::chrono::duration<float> elapsed_seconds = now - clock_;

                return elapsed_seconds.count() > t;
            }

            bool brain(std::vector<std::string> map) final {
                if (!checkDanger(map) || checkTimer(0.3)) {
                    if (getCenter_() == objective_ || checkTimer(0.4)) {
                        keep_ = false;
                        clock_ = std::chrono::system_clock::now();
                        hide(map, 1, Directions(std::rand() % 3));
                    }
                }
                if (getCenter_() != objective_) {
                    if (direction_== UP)
                        move(90.0, Directions::UP);
                    else if (direction_== DOWN)
                        move(-90.0, Directions::DOWN);
                    else if (direction_== LEFT)
                        move(0.0, Directions::LEFT);
                    else if (direction_== RIGHT)
                        move(180.0, Directions::RIGHT);
                } else
                    keep_ = false;
                return safe_;
            }

            bool checkDanger(std::vector<std::string> map);

    private:
        irr::core::vector3df getCenter_() {
            int playerX = node_->getPosition().X + 1;
            int playerZ = node_->getPosition().Z + 1;
            int x = playerX;
            int y = playerZ;

            x = ((x % 2) ? playerX - 1 : playerX);
            y = ((y % 2) ? playerZ - 1 : playerZ);
            return irr::core::vector3df(x, -1, y);
        }

        std::chrono::system_clock::time_point clock_ = std::chrono::system_clock::now();
        irr::core::vector3df objective_ = irr::core::vector3df(getCenter_().X-4, -1, getCenter_().Z);
        Directions direction_ = Directions::UP;
        bool safe_ = false;
        bool keep_ = false;
    };
}

#endif /* !IA_HPP_ */
