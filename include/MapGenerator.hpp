/*
** EPITECH PROJECT, 2019
** OOP_indie_studio_2018
** File description:
** main
*/

#ifndef BOMBERMAN_MAPGENERATOR_HPP
#define BOMBERMAN_MAPGENERATOR_HPP

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>

namespace indie
{
    class MapGenerator
    {
        public:
            MapGenerator()
            {
                mapFile_.open("map.txt");
                std::srand(std::time(nullptr));
                print_map();
                mapFile_.close();
            }

            ~MapGenerator() = default;

        private:
            void iron_line()
            {
                for (int n = 0; n < 15; n++)
                    mapFile_ << "X";
                mapFile_ << std::endl;
            }

            void empty_line()
            {
                mapFile_ << "X";
                for (int i = 0; i < 13; i++) {
                    if ((std::rand() % 20) > 3)
                        mapFile_ << "*";
                    else
                        mapFile_ << " ";
                }
                mapFile_ << "X" << std::endl;
            }

            void starter_mix_line()
            {
                mapFile_ << "X";
                mapFile_ << " ";
                mapFile_ << "X";
                for (int i = 0; i < 5; i++) {
                    if ((std::rand() % 20) > 2)
                        mapFile_ << "*";
                    else
                        mapFile_ << " ";
                    mapFile_ << "X";
                }
                mapFile_ << " ";
                mapFile_ << "X" << std::endl;
            }

            void classic_line()
            {
                mapFile_ << "X";
                for (int i = 0; i < 6; i++) {
                    if ((std::rand() % 20) > 2)
                        mapFile_ << "*";
                    else
                        mapFile_ << " ";
                    mapFile_ << "X";
                }
                if ((std::rand() % 20) > 2)
                    mapFile_ << "*";
                else
                    mapFile_ << " ";
                mapFile_ << "X" << std::endl;
            }

            void player_line()
            {
                int randVar = 0;

                mapFile_ << "X";
                mapFile_ << "P";
                mapFile_ << " ";
                for (int i = 0; i < 9; i++) {
                    randVar = (std::rand() % 20);
                    if ((std::rand() % 20) > 3)
                        mapFile_ << "*";
                    else
                        mapFile_ << " ";
                }
                mapFile_ << " ";
                mapFile_ << "P";
                mapFile_ << "X" << std::endl;
            }

            void print_map()
            {
                iron_line();
                player_line();
                starter_mix_line();
                for (int i = 0; i < 4; i++) {
                    empty_line();
                    if (i != 3)
                        classic_line();
                }
                starter_mix_line();
                player_line();
                iron_line();
            }

        private:
            std::ofstream mapFile_;

    };
}

#endif //BOMBERMAN_MAPGENERATOR_HPP
