//
// Created by mrafael on 6/14/19.
//

#ifndef BOMBERMAN_PARAMETER_HPP
#define BOMBERMAN_PARAMETER_HPP

#include <iostream>
#include <irrlicht.h>
#include <map>
#include <vector>
#include <array>
#include "Character/Player.hpp"

namespace indie {
    class Parameter {
    public:
        Parameter() {
        }

        ~Parameter() = default;

        std::string getMusic() { return musics_[selected_]; }

        irr::core::dimension2d<irr::u32> getRes() { return resolutions_[selected_res_]; }

        int getMusicVolume() { return music_volume_; }

        int getEffectVolume() { return effect_volume_; }

        bool getDuo() { return multiplayer_; }

        std::map<std::string, irr::EKEY_CODE> getMap1() { return keymap_p1; }

        std::map<std::string, irr::EKEY_CODE> getMap2() { return keymap_p2; }

        bool getFullscreen() { return fullscreen_; }

        std::vector<std::shared_ptr<ACharacter>> *getPlayers() { return pl_; }

        void setFullscreen(bool status) { fullscreen_ = status; }

        void setDuo(bool status) { multiplayer_ = status; }

        void setMusicVolume(int volume) { music_volume_ = volume; }

        void setEffectVolume(int volume) { effect_volume_ = volume; }

        void setPlayers(std::vector<std::shared_ptr<ACharacter>> *vector)
        {
            pl_ = vector;
        }

        void setMusic(int next) {
            selected_ += next;
            if (selected_ > 3)
                selected_ = 0;
            else if (selected_ < 0)
                selected_ = 3;
        }

        void setResolution(int next) {
            selected_res_ += next;
            if (selected_res_ > musics_.max_size())
                selected_res_ = 0;
            else if (selected_res_ < 0)
                selected_res_ = musics_.max_size();
        }

    private:
        bool multiplayer_ = false;
        bool fullscreen_ = false;
        std::vector<std::shared_ptr<indie::ACharacter>> *pl_;
        std::map<std::string, irr::EKEY_CODE> keymap_p1 = {
                {"Up",    irr::KEY_KEY_Z},
                {"Down",  irr::KEY_KEY_S},
                {"Left",  irr::KEY_KEY_Q},
                {"Right", irr::KEY_KEY_D},
                {"Bomb",  irr::KEY_SPACE},
        };
        std::map<std::string, irr::EKEY_CODE> keymap_p2 = {
                {"Up",    irr::KEY_UP},
                {"Down",  irr::KEY_DOWN},
                {"Left",  irr::KEY_LEFT},
                {"Right", irr::KEY_RIGHT},
                {"Bomb",  irr::KEY_RSHIFT},
        };
        int music_volume_ = 100;
        int effect_volume_ = 100;
        int selected_ = 0;
        std::vector<std::string> musics_ = {"game1",
                                            "game2",
                                            "game3",
                                            "game4",
        };
        int selected_res_ = 0;
        std::vector<irr::core::dimension2d<irr::u32>> resolutions_ = {irr::core::dimension2d<irr::u32>(1920, 1080),
                                                                      irr::core::dimension2d<irr::u32>(1600, 900),
                                                                      irr::core::dimension2d<irr::u32>(1280, 720)};
    };
}


#endif //BOMBERMAN_PARAMETER_HPP
