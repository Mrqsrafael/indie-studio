/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Game
*/

#ifndef GAME_HPP_
#define GAME_HPP_

#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <memory>

#include "AScene.hpp"
#include "Node/Item/Bomb.hpp"
#include "Node/Map.hpp"
#include "IAnimatedMeshSceneNode.h"
#include "Core/Core.hpp"
#include "Indie.hpp"


namespace indie
{
	class Game : public AScene
	{
		public:
			Game(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) :
			    AScene(device, s, p),
			    map_(new Map(scene_manager_->getRootSceneNode(), scene_manager_, p))
            {
                sound_.playSound(params_->getMusic());
                scene_manager_->addCameraSceneNode(scene_manager_->getRootSceneNode(),
                        irr::core::vector3df(21,21, 14),
                        irr::core::vector3df(13,0,14));
                // scene_manager_->addCameraSceneNodeMaya(scene_manager_->getRootSceneNode());
            }

            ~Game() final
            {
			    delete map_;
            }

            Game(const Game&) = delete;
            Game& operator=(const Game&) = delete;

	    public:
	        bool is_bomb(irr::core::vector3df position)
	        {
                for (auto &p : map_->getPayers()) {
                    for (auto it = p->getBombs().rbegin(); it != p->getBombs().rend(); ++it) {
                        if (!(*it)->used && position == (*it)->explode())
                            return false;
                    }
                }
                return true;
            }

            int pushWin() {
                sound_.stopSound(params_->getMusic());
                return (sceneAction::winScreen);
            }

            int pushLoose() {
                sound_.stopSound(params_->getMusic());
                return (sceneAction::looseScreen);
            }

            int win_loose()
            {
                if (params_->getDuo()) {
                    if ((map_->getPayers().at(0)->getRank() == 1 || map_->getPayers().at(1)->getRank() == 1)
                    && first_) {
                        first_ = false;
                        params_->setPlayers(&map_->getPayers());
                        return pushWin();

                    }
                    if (map_->getPayers().at(0)->getDead() && map_->getPayers().at(1)->getDead()
                    && first_) {
                        first_ = false;
                        params_->setPlayers(&map_->getPayers());
                        return pushLoose();
                    }
                } else {
                    if (map_->getPayers().at(0)->getRank() == 1 && first_) {
                        first_ = false;
                        params_->setPlayers(&map_->getPayers());
                        return pushWin();

                    }
                    if (map_->getPayers().at(0)->getDead() && first_) {
                        first_ = false;
                        params_->setPlayers(&map_->getPayers());
                        return pushLoose();
                    }
                }
                return 0;
            }

            void checkDead(std::array<int, 4> deadTable)
            {
                irr::core::vector3df node_position = {0,0,0};
                int i = 3;
                if (rank_ == 1) {
                    for (auto &pl : map_->getPayers())
                        if (pl->getRank() == 0)
                            pl->setRank(1);
                }
                for (auto p = map_->getPayers().rbegin(); p != map_->getPayers().rend(); p++) {
                    node_position = map_->getPayers().at(i)->getPlayerPosition();
                    if (deadTable[i] == 1) {
                        node_position.Y -= 4;
                        map_->getPayers().at(i)->setPlayerPosition(node_position);
                        map_->getPayers().at(i)->setDead(true);
                        map_->getPayers().at(i)->setRank(rank_);
                        sound_.playSound("player_out");
                        rank_--;
                    }
                    i--;
                }
            }

            int manage_event(const EventReceiver &event_receiver) final
            {
                if (event_receiver.IsKeyDown(irr::KEY_ESCAPE)) {
                    return (sceneAction::addGamePause);
                }
                map_->checkMap(sound_);
                std::array<int, 4> particles;
                std::array<int, 4> tabDeath;
                std::map<std::string, irr::EKEY_CODE> *keymap;
                irr::core::vector3df bomb;
                int kills = 0;
                int end_party;

                for (auto p  = map_->getPayers().begin(); p != map_->getPayers().end(); p++) {
                    for (auto it = (*p)->getBombs().rbegin(); it != (*p)->getBombs().rend(); it++) {
                        if ((*it)->checkTimer() && !(*it)->used) {
                            sound_.playSound("bomb_ex");
                            bomb = (*it)->explode();
                            particles = map_->destroy(bomb, (*p)->getRange());
                            (*it)->doParticles(particles);
                            tabDeath = map_->is_player(bomb, particles);
                            for (auto tab : tabDeath)
                                kills += tab;
                            (*p)->addKill(kills);

                        }
                        if ((*it)->used && (*it)->checkParticle()) {
                            std::advance(it, 1);
                            (*p)->getBombs().erase(it.base());
                            std::advance(it, -1);
                        }
                    }
                    checkDead(tabDeath);
                    tabDeath = {0,0,0,0};
                    end_party = win_loose();
                    if (end_party)
                        return end_party;
                }
                for (auto p  = map_->getPayers().begin(); p != map_->getPayers().end(); p++) {
                    if ((*p)->getMap() && (*p)->getDead() == false) {
                        keymap = (*p)->getMap();
                        if (event_receiver.IsKeyDown(keymap->at("Bomb")) && is_bomb(map_->getCenter((*p)->getPlayerPosition())))
                            if ((*p)->addBomb(node_, scene_manager_, 1, map_->getCenter((*p)->getPlayerPosition()))) {
                                sound_.playSound("bomb_set");
                                map_->getMap()[map_->getCenter((*p)->getPlayerPosition()).X/2][map_->getCenter((*p)->getPlayerPosition()).Z/2] = 'o';
                            }
                        if (event_receiver.IsKeyDown(keymap->at("Up")))
                            (*p)->move(90.0, Directions::UP);
                        else if (event_receiver.IsKeyDown(keymap->at("Down")))
                            (*p)->move(-90.0, Directions::DOWN);
                        else if (event_receiver.IsKeyDown(keymap->at("Left")))
                            (*p)->move(0.0, Directions::LEFT);
                        else if (event_receiver.IsKeyDown(keymap->at("Right")))
                            (*p)->move(180.0, Directions::RIGHT);
                        else
                            (*p)->standUp();
                    } else if ((*p)->getDead() == false){
                        if ((*p)->brain(map_->getMap()) && is_bomb(map_->getCenter((*p)->getPlayerPosition())))
                            if ((*p)->addBomb(node_, scene_manager_, 1, map_->getCenter((*p)->getPlayerPosition()))) {
                                sound_.playSound("bomb_set");
                                map_->getMap()[map_->getCenter((*p)->getPlayerPosition()).X / 2][
                                        map_->getCenter((*p)->getPlayerPosition()).Z / 2] = 'o';
                            }
                    }
                }
                return 0;
            }

		private:
            bool first_ = true;
            unsigned int rank_ = 4;
	        Map *map_ = nullptr;
			irr::scene::ISceneCollisionManager *collman = nullptr;

	};
}

#endif /* !GAME_HPP_ */