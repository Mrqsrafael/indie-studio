/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Option
*/

#ifndef OPTION_HPP_
#define OPTION_HPP_

#include <algorithm>
#include <unordered_map>
#include "AScene.hpp"

namespace indie
{
	class Option : public AScene
	{
		public:
			explicit Option(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p);
			~Option() override;
			std::unordered_map<std::string, irr::gui::IGUIButton *> buttonList;
			std::unordered_map<std::string, irr::gui::IGUIScrollBar*> scroolList;
			std::unordered_map<std::string, irr::gui::IGUIStaticText*> textList;
			std::unordered_map<std::string, irr::gui::IGUIImage *> imageList;

		public:
			int manage_event(const EventReceiver &event_receiver) final;
			void create_gui(irr::IrrlichtDevice *device);
			void draw() override;
			virtual irr::gui::IGUIEnvironment* getGui() { return this->guienv;}
			virtual void setGui(irr::gui::IGUIEnvironment* env) {this->guienv = env;}

		private:
			irr::gui::IGUIEnvironment* guienv = nullptr;
	};
}

#endif /* !OPTION_HPP_ */
