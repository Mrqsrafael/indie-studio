/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Victory
*/

#ifndef VICTORY_HPP_
#define VICTORY_HPP_

#include "IMeshSceneNode.h"
#include "AScene.hpp"
#include "Events/EventReceiver.hpp"
#include "Character/ACharacter.hpp"
#include "Character/Player.hpp"


namespace indie
{
	class Victory : public AScene {
		public:
			explicit Victory(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) : AScene(device, s, p) {
				sound_.playSound("win");
				printScore();
				create_gui(device_);
			}
			~Victory() override {
                std::for_each(this->buttonList.begin(), this->buttonList.end(),
                              [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                                  if (p.second)
                                      p.second->remove();
                              });
                std::for_each(this->imageList.begin(), this->imageList.end(),
                              [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
                                  if (p.second)
                                      p.second->remove();
                              });
			}

			int manage_event(const EventReceiver &event_receiver);

			void create_gui(irr::IrrlichtDevice *device);

			void draw();
			void printScore();
			virtual irr::gui::IGUIEnvironment* getGui() { return this->guienv;}
			virtual void setGui(irr::gui::IGUIEnvironment* env) {this->guienv = env;}

		protected:
		private:
			std::unordered_map<std::string, irr::gui::IGUIButton *> buttonList;
			std::unordered_map<std::string, irr::gui::IGUIImage *> imageList;
			std::map<unsigned int , std::pair<std::string, unsigned int>> score_;
			irr::gui::IGUIButton *button_;
			irr::gui::IGUIImage *image_;
			irr::gui::IGUIEnvironment* guienv;
	};
}

#endif /* !RESULT_HPP_ */
