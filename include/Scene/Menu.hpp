/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Menu
*/

#ifndef MENU_HPP_
#define MENU_HPP_

#include <unordered_map>
#include "IMeshSceneNode.h"
#include "AScene.hpp"
#include "Events/EventReceiver.hpp"
#include <map>

namespace indie
{
	class Menu : public AScene
	{
		public:
			explicit Menu(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p);
			~Menu() override = default;
			std::unordered_map<std::string, irr::gui::IGUIButton *> buttonList;
			std::unordered_map<std::string, irr::gui::IGUIImage *> imageList;

		public:
			int manage_event(const EventReceiver &event_receiver) final;
			void create_gui(irr::IrrlichtDevice *device);
			void draw() override;
			virtual irr::gui::IGUIEnvironment* getGui() { return this->guienv;}
			virtual void setGui(irr::gui::IGUIEnvironment* env) {this->guienv = env;}

		private:
			irr::gui::IGUIEnvironment* guienv;
	};
}

#endif /* !MENU_HPP_ */
