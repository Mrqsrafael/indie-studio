/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** IScene
*/

#ifndef ISCENE_HPP_
#define ISCENE_HPP_

#include "Indie.hpp"
#include "Events/EventReceiver.hpp"

namespace indie
{
	class IScene
	{
		public:
			virtual ~IScene() {}

		public:
			virtual void draw() = 0;
			virtual int manage_event(const EventReceiver &event_receiver) = 0;
	};
}

#endif /* !ISCENE_HPP_ */
