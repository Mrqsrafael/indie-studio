/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** GamePause
*/

#ifndef GAMEPAUSE_HPP_
#define GAMEPAUSE_HPP_

#include <unordered_map>
#include "IMeshSceneNode.h"
#include "AScene.hpp"
#include "Events/EventReceiver.hpp"
#include <map>

namespace indie
{
	class GamePause : public AScene
	{
		public:
			explicit GamePause(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p);
			~GamePause() override;
			std::unordered_map<std::string, irr::gui::IGUIButton *> buttonList;
			std::unordered_map<std::string, irr::gui::IGUIImage *> imageList;

		public:
			int manage_event(const EventReceiver &event_receiver) final;
			void create_gui(irr::IrrlichtDevice *device);
			void draw() override;
			virtual irr::gui::IGUIEnvironment* getGui() { return this->guienv;}
			virtual void setGui(irr::gui::IGUIEnvironment* env) {this->guienv = env;}

		private:
			irr::gui::IGUIEnvironment* guienv;
	};
}

#endif /* !GAMEPAUSE_HPP_ */
