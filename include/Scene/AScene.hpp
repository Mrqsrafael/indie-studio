/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** AScene
*/

#ifndef SCENE_HPP_
#define SCENE_HPP_

#include <IrrlichtDevice.h>
#include <ISceneManager.h>
#include <ISceneNode.h>
#include <IAnimatedMeshSceneNode.h>

#include "IScene.hpp"
#include "Core/SoundManager.hpp"
#include "Parameter.hpp"

namespace indie
{
    class AScene : public IScene
    {
        public:
            explicit AScene(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> parameter);

            ~AScene() override
            {
                if (node_) {
                    node_->remove();
                    node_ = nullptr;
                }
            }

        public:
            void draw() override;
            int manage_event(const EventReceiver &event_receiver) override = 0;

        protected:
            irr::IrrlichtDevice *device_ = nullptr;
            irr::scene::ISceneManager *scene_manager_ = nullptr;
            irr::scene::ISceneNode *node_ = nullptr;
            SoundManager sound_;
            std::shared_ptr<Parameter> params_;
    };
}

#endif /* !SCENE_HPP_ */
