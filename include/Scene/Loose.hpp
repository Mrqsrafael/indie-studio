/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Victory
*/

#ifndef LOOSE_HPP_
#define LOOSE_HPP_

#include "IMeshSceneNode.h"
#include "AScene.hpp"
#include "Events/EventReceiver.hpp"
#include "Character/ACharacter.hpp"


namespace indie
{
	class Loose : public AScene {
		public:
			explicit Loose(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p) : AScene(device, s, p) {
				sound_.playSound("loose");
				printScore();
				create_gui(device_);
			}
			~Loose() override {
                std::for_each(this->buttonList.begin(), this->buttonList.end(),
                              [](const std::pair<std::string,  irr::gui::IGUIButton *> &p) {
                                  if (p.second)
                                      p.second->remove();
                              });
                std::for_each(this->imageList.begin(), this->imageList.end(),
                              [](const std::pair<std::string,  irr::gui::IGUIImage *> &p) {
                                  if (p.second)
                                      p.second->remove();
                              });
            }
			std::unordered_map<std::string, irr::gui::IGUIButton *> buttonList;
			std::unordered_map<std::string, irr::gui::IGUIImage *> imageList;

			void printScore();
			int manage_event(const EventReceiver &event_receiver);
			void create_gui(irr::IrrlichtDevice *device);
			void draw();

			virtual irr::gui::IGUIEnvironment* getGui() { return this->guienv;}
			virtual void setGui(irr::gui::IGUIEnvironment* env) {this->guienv = env;}

		protected:
		private:
			irr::gui::IGUIButton *button_;
			irr::gui::IGUIImage *image_;
			irr::gui::IGUIEnvironment* guienv;
			std::map<unsigned int , std::pair<Player, unsigned int>> score_;
	};
}

#endif /* !RESULT_HPP_ */
