/*
** EPITECH PROJECT, 2019
** indie-studio
** File description:
** Menu_Play
*/

#ifndef MENU_PLAY_HPP_
#define MENU_PLAY_HPP_

#include <unordered_map>
#include "IMeshSceneNode.h"
#include "AScene.hpp"
#include "Events/EventReceiver.hpp"

namespace indie
{
	class Menu_Play : public AScene
	{
		public:
			explicit Menu_Play(irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p);
			~Menu_Play() override;
			std::unordered_map<std::string, irr::gui::IGUIButton *> buttonList;
			std::unordered_map<std::string, irr::gui::IGUIImage *> imageList;

		public:
			int manage_event(const EventReceiver &event_receiver) final;
			void create_gui(irr::IrrlichtDevice *device);
			void draw() override;
			virtual irr::gui::IGUIEnvironment* getGui() { return this->guienv;}
			virtual void setGui(irr::gui::IGUIEnvironment* env) {this->guienv = env;}

		private:
			irr::gui::IGUIEnvironment* guienv = nullptr;
	};
}

#endif /* !MENU_PLAY_HPP_ */
