#ifndef BOMBERMAN_INTRODUCTION_HPP
#define BOMBERMAN_INTRODUCTION_HPP

#include "AScene.hpp"
#include "Node/BombMenu.hpp"
#include "Core/SoundManager.hpp"


namespace indie
{
    class Introduction : public AScene
    {
        public:
            Introduction (irr::IrrlichtDevice *device, SoundManager &s, std::shared_ptr<Parameter> p)
                :   AScene(device, s, p)
            {
                sound_.playSound("menu2");
                create_scene();
                create_gui(device);
                std::srand(std::time(nullptr));
            }

            ~Introduction() override
            {
                if (animated_node_) {
                    animated_node_->remove();
                    animated_node_ = nullptr;
                }
                erase_bombs();
            }

        public:
            void create_gui(irr::IrrlichtDevice *device);
            void create_scene();
            void put_bomb();

            void erase_bombs();
            int manage_event(const EventReceiver &event_receiver) override;
            void character_walk();

            virtual irr::gui::IGUIEnvironment* getGui()
            { return this->guienv; }

            virtual void setGui(irr::gui::IGUIEnvironment* env)
            { this->guienv = env; }

            void draw() final;

        private:
            irr::gui::IGUIEnvironment* guienv = nullptr;
            irr::gui::IGUIImage *image_ = nullptr;
            irr::scene::IAnimatedMeshSceneNode *animated_node_ = nullptr;
            std::vector<std::unique_ptr<BombM>> bombs_;
            bool start_ = false;
            std::chrono::system_clock::time_point clock_ = std::chrono::system_clock::now();
            std::chrono::system_clock::time_point walk_clock_ = std::chrono::system_clock::now();
    };
}

#endif //BOMBERMAN_INTRODUCTION_HPP
